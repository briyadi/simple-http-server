// Import HTTP Module
const http = require("http")
// Import File System Module
const fs = require("fs")

// fungsi untuk menghandle request
// dari user ...
function handleRequest(req, res) {
    // logging request yang dikirim oleh user
    console.log(req.method, req.url)

    // pengecekan url
    if (req.url == "/person") {

        switch(req.method) {
            // READ
            case "GET":
                // jika sudah ada file person.json maka tampilkan
                // selain itu kirim response 404 (not found)
                if (fs.existsSync("./person.json")) {
                    let person = fs.readFileSync("./person.json")

                    res.writeHead(200, {'content-type': 'application/json'})
                    res.end(person.toString())
                    return
                }
                res.writeHead(404, {'content-type': 'application/json'})
                res.end(`{"error": "not found"}`)
            break;
            // CREATE
            case "POST":
                // jika belum ada file person.json maka buat dan isi
                // selain itu kirim response 409 (duplicate)
                if (!fs.existsSync("./person.json")) {
                    let personString = ""
                    req.on("data", function(body) {
                        personString = body.toString()
                    })
                    req.on("end", function() {
                        // write file ...
                        fs.writeFileSync("./person.json", personString)

                        res.writeHead(200, {'content-type': 'application/json'})
                        res.end(personString)
                    })
                    return
                }

                res.writeHead(409, {'content-type': 'application/json'})
                res.end(`{"error": "person already exists"}`)
            break;
            // UPDATE (PUT & PATCH)
            case "PUT":
                // jika sudah ada file person.json maka perbarui
                // dan berikan response 200 (ok)
                // selain itu 404 (not found)

                if (fs.existsSync("./person.json")) {
                    let personString = ""
                    req.on("data", function(body) {
                        personString = body.toString()
                    })
                    req.on("end", function() {
                        // ambil data lama
                        let original = fs.readFileSync("./person.json")
                        let oldPerson = JSON.parse(original.toString())
                        let newPerson = JSON.parse(personString)

                        // lakukan compare data
                        let person = Object.assign(oldPerson, newPerson)

                        // convert data baru menjadi json string
                        let personJSON = JSON.stringify(person, null, 4)

                        // update file ...
                        fs.writeFileSync("./person.json", personJSON)

                        res.writeHead(200, {'content-type': 'application/json'})
                        res.end(personJSON)
                    })
                    // pastikan bahwa tidak ada kode yang 
                    // di eksekusi setelah return
                    return
                }

                res.writeHead(404, {'content-type': 'application/json'})
                res.end(`{"error": "not found"}`)
            break;
            // DELETE
            case "DELETE":
                // jika sudah ada file person.json maka hapus file tsb
                // selain itu kirim response 404 (not found)

                if (fs.existsSync("./person.json")) {

                    // hapus file fisiknya
                    fs.unlinkSync("./person.json")

                    res.writeHead(200, {'content-type': 'application/json'})
                    res.end(`{"message": "success"}`)
                    // pastikan bahwa tidak ada kode yang
                    // di eksekusi setelah return
                    return
                }

                res.writeHead(404, {'content-type': 'application/json'})
                res.end(`{"error": "not found"}`)
            break;
            default:
                res.writeHead(404)
                res.end("Not found")
            break;
        }

        return
    }

    // jika url selain /person dinyatakan sebagai not found
    res.writeHead(404)
    res.end("Not found")
}

// Persiapkan HTTP server
const server = http.createServer(handleRequest)

// Jalankan HTTP Server
// Diusahakan port > 1000 untuk development di local
server.listen(3000, function() {
    console.log("Server running on port 3000")
})

// buka localhost:PORT, misal http://localhost:3000/hello
// http://123.123.123.123:3000